# MQA URL Checker

## Setup

1. Install all of the following software
        
        * Java JDK >= 1.8
        * Git >= 2.17
  
2. Clone the directory and enter it
    
        git@gitlab.fokus.fraunhofer.de:viaduct/mqa-url-checker.git
        
3. Edit the environment variables in the `Dockerfile` to your liking. Variables and their purpose are listed below:
   
    |Key|Description|Default|
    |:--- |:---|:---|
    |PORT| The port this service will run on | 8085 |
    |WORKER_COUNT| The number of UrlCheckerVerticles to spawn | 3 |
    |HTTP_USER_AGENT| The user agent to send with the check requests | Mozilla/5.0 (European Data Portal) Gecko/20100101 Firefox/40.1 | 
    |TIMEOUT_IN_MILLIS| HTTP request timeout in milliseconds | 10000 |
    |CONNECTION_EXCEPTION_STATUS| The status code to set in case of exceptions | 1100 |
    |INVALID_URL_STATUS| The status code to set if invalid URLs are provided | 1300 | 
    |INVALID_URL_MESSAGE| The status message to set if invalid URLs are provided | Invalid URL |
## Run

### Production

Build the project by using the provided Maven wrapper. This ensures everyone this software is provided to can use the exact same version of the maven build tool.
The generated _fat-jar_ can then be found in the `target` directory.

* Linux
    
        ./mvnw clean package
        java -jar target/url-checker-0.1-fat.jar

* Windows

        mvnw.cmd clean package
        java -jar target/url-checker-0.1-fat.jar
      
* Docker
    
        1. Start your docker daemon 
        2. Build the application as described in Windows or Linux
        3. Adjust the port number (`EXPOSE` in the `Dockerfile`)
        4. Build the image: `docker build -t viaduct/url-checker .`
        5. Run the image, adjusting the port number as set in step 3: `docker run -i -p 8085:8085 edp/mqa-url-checker`
        6. Configuration can be changed without rebuilding the image by overriding variables: `-e PORT=8086`

### Development

For use in development two scripts are provided in the project's root folder. These enable hot deployment (dynamic recompiling when changes are made to the source code).
Linux users should run the `redeploy.sh` and Windows users the `redeploy.bat` file.

## CI

The repository uses the gitlab builtin CI Framework. 
The `gitlab-ci.yaml` file contains the appropriate config. With each push a new docker image is built using the `Dockerfile`.
The image is then stored in the gitlab registry, from which it can then be pulled using the following command:

    docker pull registry.gitlab.com/european-data-portal/mqa-url-checker

## API

A formal OpenAPI 3 specification can be found in the `src/main/resources/webroot/openapi.yaml` file.
A visually more appealing version is available at `{url}:{port}` once the application has been started.


## Design decisions

|Date|Decision|
|:--- |:--- |


## Known problems 

|Date|Problem|
|:--- |:--- |
|12.6.18 | Openapi : In the yaml file the keys have to be clear of '' and "" otherwise stuff will fail on certain machines.
