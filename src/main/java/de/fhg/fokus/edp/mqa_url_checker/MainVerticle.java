package de.fhg.fokus.edp.mqa_url_checker;

import io.vertx.config.ConfigRetriever;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.CompositeFuture;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.json.DecodeException;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.api.contract.openapi3.OpenAPI3RouterFactory;
import io.vertx.ext.web.handler.StaticHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;


public class MainVerticle extends AbstractVerticle {

    private static final Logger LOG = LoggerFactory.getLogger(MainVerticle.class);

    private JsonObject config;

    @Override
    public void start(Future<Void> future) {

        LOG.info("Launching URL-Checker...");

        // startup is only successful if no step failed
        Future<Void> steps = loadConfig()
                .compose(handler -> bootstrapVerticles())
                .compose(handler -> startServer());

        steps.setHandler(handler -> {
            if (handler.succeeded()) {
                LOG.info("URL-Checker successfully launched");
            } else {
                LOG.error("Failed to launch Transformer: " + handler.cause());
            }
        });
    }

    private Future<Void> loadConfig() {
        Future<Void> future = Future.future();

        ConfigRetriever.create(vertx).getConfig(handler -> {
            if (handler.succeeded()) {
                config = handler.result();
                LOG.info(config.encodePrettily());
                future.complete();
            } else {
                future.fail("Failed to load config: " + handler.cause());
            }
        });

        return future;
    }

    private CompositeFuture bootstrapVerticles() {
        DeploymentOptions options = new DeploymentOptions()
                .setInstances(config.getInteger(ApplicationConfig.ENV_WORKER_COUNT, ApplicationConfig.DEFAULT_WORKER_COUNT))
                .setConfig(config)
                .setWorker(true);

        Future<Void> future = Future.future();

        vertx.deployVerticle(UrlCheckerVerticle.class.getName(), options, handler -> {
            if (handler.succeeded()) {
                future.complete();
            } else {
                handler.cause().printStackTrace();
                LOG.error("Failed to deploy verticle [{}] : {}", UrlCheckerVerticle.class.getName(), handler.cause());
                future.fail("Failed to deploy [" + UrlCheckerVerticle.class.getName() + "] : " + handler.cause());
            }
        });

        List<Future> deploymentFutures = new ArrayList<>();
        deploymentFutures.add(future);

        return CompositeFuture.join(deploymentFutures);
    }

    private Future<Void> startServer() {
        Future<Void> startFuture = Future.future();
        Integer port = config.getInteger(ApplicationConfig.ENV_APPLICATION_PORT, ApplicationConfig.DEFAULT_APPLICATION_PORT);

        OpenAPI3RouterFactory.create(vertx, "webroot/openapi.yaml", handler -> {
            if (handler.succeeded()) {
                OpenAPI3RouterFactory routerFactory = handler.result();
//                RouterFactoryOptions options = new RouterFactoryOptions().setMountNotImplementedHandler(true).setMountValidationFailureHandler(true);
//                routerFactory.setOptions(options);

                routerFactory.addHandlerByOperationId("checkUrl", this::checkUrl);

                Router router = routerFactory.getRouter();
                router.route("/*").handler(StaticHandler.create());

                HttpServer server = vertx.createHttpServer(new HttpServerOptions().setPort(port));
                server.requestHandler(router).listen();

                LOG.info("Server successfully launched on port [{}]", port);
                startFuture.complete();
            } else {
                // Something went wrong during router factory initialization
                LOG.error("Failed to start server at [{}]: {}", port, handler.cause());
                startFuture.fail(handler.cause());
            }
        });

        return startFuture;
    }


    // passes urlCheckRequest to worker and handles response
    private void checkUrl(RoutingContext routingContext) {
        if (routingContext.getBodyAsString() != null) {
            // attempt decoding to detect malformed requests
            try {
                UrlCheckRequest request = Json.decodeValue(routingContext.getBodyAsString(), UrlCheckRequest.class);
                LOG.debug("Received valid {}", request.toString());

                vertx.eventBus().send(ApplicationConfig.MSG_URL_CHECK_ADDR, routingContext.getBodyAsString());

                routingContext.response()
                        .setStatusCode(202) // accepted
                        .putHeader("Content-Type", "application/json")
                        .end(routingContext.getBodyAsString());

            } catch (DecodeException e) {
                LOG.warn("Exception thrown while decoding request {}: {}", routingContext.getBodyAsString(), e.getMessage());
                routingContext.response().setStatusCode(400).end("Malformed request");
            }
        } else {
            routingContext.response().setStatusCode(400).end("No request body provided");
        }
    }
}
